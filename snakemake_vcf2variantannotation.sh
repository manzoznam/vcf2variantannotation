#!/bin/bash

# Get the input directory from the first argument
find_dir=$1

# Find all vcf files that match the requirements and store them in an array
vcf_files_S5=($(find "$find_dir" -name "*vcf" | grep -v "pyrepair" | grep -v "gnomad_MAF" | grep -v "removed" | grep "_Filtered"))
vcf_files_genexus=($(find "$find_dir" -name "*vcf" | grep -v "pyrepair" | grep -v "gnomad_MAF" | grep -v "Non-Filtered" | grep -v "removed" | grep "O.*vcf"))

vcf_files=("${vcf_files_S5[@]}" "${vcf_files_genexus[@]}")
echo "${vcf_files[@]}"

# Iterate over each vcf file and execute snakemake
for input_vcf in "${vcf_files[@]}"; do
	echo "Working on $input_vcf"
	snakemake --cores 8 --config config.yaml --config input_file="$input_vcf"
done
