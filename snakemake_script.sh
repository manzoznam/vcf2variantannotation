#!/usr/bin/bash

find_dir=$1
#input_vcf=$1
vcf_dir="$(dirname $input_vcf)"

find "$find_dir" -name "*vcf" | grep -v "pyrepair" | grep -v "gnomad_MAF" | grep -v "removed" | grep "_Filtered" | xargs -P 1 -I {} bash snakemake_script.sh {}
find "$find_dir" -name "*vcf" | grep -v "pyrepair" | grep -v "gnomad_MAF" | grep -v "removed" | grep "O.*vcf" | xargs -P 1 -I {} bash snakemake_script.sh {}

# python3  
snakemake --cores 8 --config config.yaml --config input_file="$input_vcf"


