import sqlite3

def individual_query_variants(db_path, table_name, variants):
    # Connect to the SQLite database
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()
    
    results = []
    for chr, pos, ref, alt in variants:
        # Prepare the SQL query for each variant
        query = f"""
        SELECT * FROM {table_name}
        WHERE CHR = '{chr}' AND POS = {pos} AND REF = '{ref}' AND ALT = '{alt}'
        """
        
        # Execute the SQL query
        cursor.execute(query)
        data = cursor.fetchall()
        results.extend(data)
    
    # Close the database connection
    conn.close()
    return results
	

def batch_query_variants(db_path, table_name, variants):
    # Connect to the SQLite database
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()
    
    # Construct the WHERE clause with all variant conditions
    conditions = " OR ".join([
        f"(CHR = '{chr}' AND POS = {pos} AND REF = '{ref}' AND ALT = '{alt}')"
        for chr, pos, ref, alt in variants
    ])
    
    # Prepare the SQL query
    query = f"SELECT * FROM {table_name} WHERE {conditions}"
    
    # Execute the SQL query
    cursor.execute(query)
    data = cursor.fetchall()
    
    # Close the database connection
    conn.close()
    
    return data

	
db_path = 'gnomad_v2_exomes.sdb'
table_name = 'gnomad_v2_exomes'
variants = [
    ('19', 66044, 'T', 'C'),
	('12', 78743, 'C', 'T'),
	('19', 107526, 'C', 'T'),
	('12', 78748, 'G', 'C'),
	('19', 107536, 'A', 'C'),
	('12', 78762, 'T', 'C'),
	('19', 107551, 'A', 'G'),
	('12', 78826, 'TCAGACCTATGCCGTGCCCCTCATCCAGC', 'T'),
	('17', 66048, 'C', 'T'),
	('19', 66044, 'T', 'C'),
	('12', 78743, 'C', 'T'),
	('17', 107526, 'C', 'T'),
	('12', 78748, 'G', 'C'),
	('19', 107536, 'A', 'C'),
	('14', 78762, 'T', 'C'),
	('19', 107551, 'A', 'G'),
	('17', 78826, 'TCAGACCTATGCCGTGCCCCTCATCCAGC', 'T'),
	('X', 66048, 'C', 'T')
]
res = batch_query_variants(db_path, table_name, variants_list)
df = pd.DataFrame(res, columns=columns)


########## CONVERT variants from vcf to tsv
'''
bcftools view -e 'ALT="<CNV>"' Oncomine_LibPrep2554_ff8081818b85f1a8018f9e7862e6015e.pyrepair.vcf > nocnv.vcf
bcftools view -e 'INFO/AF >= 0.01' nocnv.vcf
bcftools query -f "%CHROM\t%POS\t%REF\t%ALT" nocnv1.vcf > nocnv.tsv
'''

file_path = "nocnv.tsv"
variants_list = []
with open(file_path, 'r') as file:
    for line in file:
        # Split the line into parts using tab as the separator
        parts = line.strip().split('\t')
        parts[0] = parts[0].replace("chr", '')
        # Convert the second element to an integer
        parts[1] = int(parts[1])
        # Append the tuple to the list
        variants_list.append(tuple(parts))


########################################### FASTER SQL search

import sqlite3

def individual_query_variants(db_path, table_name, variants):
    # Connect to the SQLite database
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()
    
    # Start a transaction
    conn.execute('BEGIN')
    
    results = []
    for chr, pos, ref, alt in variants:
        # Prepare the parameterized SQL query for each variant
        query = f"""
        SELECT * FROM {table_name}
        WHERE CHR = ? AND POS = ? AND REF = ? AND ALT = ?
        """
        
        # Execute the SQL query with parameters
        cursor.execute(query, (chr, pos, ref, alt))
        data = cursor.fetchall()
        results.extend(data)
    
    # Commit the transaction
    conn.commit()
    
    # Close the database connection
    conn.close()
    
    return results


