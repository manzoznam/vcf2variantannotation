# Find and annotate
find_dir=$1
#find "$find_dir" -name "*vcf" | grep -v "pyrepair" | grep -v "gnomad_MAF" | grep -v "removed" | grep "_Filtered" | xargs -P 6 -I {} bash snakemake_script.sh {}
find "$find_dir" -name "*vcf" | grep -v "pyrepair" | grep -v "gnomad_MAF" | grep -v "removed" | grep "O.*vcf" | xargs -P 1 -I {} bash snakemake_script.sh {}

