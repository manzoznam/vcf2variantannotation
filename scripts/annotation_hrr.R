#!/usr/bin/R
source("~/github_app/GDrive_VariantReport/Gauths.R")

# BOILERPLATE
library(optparse)

option_list = list(
  make_option(c("-f", "--file"), type="character", default=NULL,
              help="parsed snv file (path)", metavar="character"),
  make_option(c("-g", "--googleid"), type="character", default=NULL,
              help="googlesheet HRR genes link ", metavar="character"))
opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser)


#################################################/ OPTPARSE

snvt = readr::read_tsv(opt$file)
googlesheetsid = googledrive::as_id(opt$googleid)


if(nrow(snvt) > 0){
	HRRgenes = googlesheets4::read_sheet(ss = googlesheetsid, sheet  = "HRR_single_genes", skip = 1)
	hrrs = VariantAnnotationModules::HRR_check_retrieve_table(snvt, HRRgenes)
	writeLines(readr::format_tsv(hrrs), stdout())

}else{
        toi = readr::read_tsv(opt$file)
        toi = tibble::as_tibble(toi)
        writeLines(readr::format_tsv(toi), stdout())
}
