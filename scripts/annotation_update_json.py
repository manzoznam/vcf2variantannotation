# update_json.py
import json
import os
import sys

def update_json(json_filepath, filepath_of_interest, key_string):
    # Check if the JSON file exists
    if os.path.isfile(json_filepath):
        # Read the existing JSON file
        with open(json_filepath, 'r') as file:
            data = json.load(file)
    else:
        # Create a new JSON file if it doesn't exist
        data = {}
    # Update the JSON data with the new key-value pair
    data[key_string] = filepath_of_interest
    # Write the updated JSON data back to the file
    with open(json_filepath, 'w') as file:
        json.dump(data, file, indent=4)

if __name__ == "__main__":
    json_filepath = sys.argv[1]
    filepath_of_interest = sys.argv[2]
    key_string = sys.argv[3]
    update_json(json_filepath, filepath_of_interest, key_string)

