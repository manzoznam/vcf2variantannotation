import sys
import json

# Check if a file path is provided as a command line argument
if len(sys.argv) < 2: 
    print("Please provide a json file path as a command line argument.")
    sys.exit(1)

# Read the json file from the command line argument
file_path = sys.argv[1]

try:
    with open(file_path, 'r') as file:
        data = json.load(file)
except FileNotFoundError:
    print(f"File not found: {file_path}")
    sys.exit(1)
except json.JSONDecodeError:
    print(f"Invalid JSON file: {file_path}")
    sys.exit(1)

filter_config = dict(
        WorkflowName = '',
        gnomad_filter_val = 0.01,
        liquid_biopsy = False,
        AF_filter = '')


# Check if 'WorkflowName' contains "Liquid Biopsy" and set the value accordingly
liquid_biopsy_panels = ['Liquid Biopsy', 'Oncomine TagSeq Breast', 'Oncomine Colon Tumor']
wfname = data['IonReporterWorkflowName']
if isinstance(wfname, str):
    wfname = wfname
else:
    wfname = wfname[0]

#if any("Liquid Biopsy" in s for s in wfname):
if any([t in wfname for t in liquid_biopsy_panels]):    
    filter_config['AF_filter'] = 0
    filter_config['liquid_biopsy'] = True
else:
    filter_config['AF_filter'] = 0.019

filter_config['WorkflowName'] = wfname
# Print the generated dictionary
json.dump(filter_config, sys.stdout, indent=4)
