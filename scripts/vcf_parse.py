import pandas as pd

# Define a function to process a line and return a dictionary of key-value pairs
def process_line(line):
    # Split the line by tab and take the 6th element
    sixth_element = line.split('\t')[7]
    # Split the 6th element by ";" and iterate over each pair
    pairs = sixth_element.split(';')
    data_dict = {}
    for pair in pairs:
        # Check if the pair contains "FUNC={("
        if "FUNC={(" in pair:
            # Extract the content inside "FUNC={(" and ")"
            func_content = pair.split('FUNC={(')[-1].split(')')[0]
            # Split the content by "," and iterate over each func pair
            func_pairs = func_content.split(',')
            for func_pair in func_pairs:
                key, value = func_pair.split('=')
                data_dict[f'FUNC_{key}'] = value
        else:
            key, value = pair.split('=')
            data_dict[key] = value
    return data_dict

# Example line from the file
example_line = "col1\\tcol2\\tcol3\\tcol4\\tcol5\\tKEY1=VALUE1;KEY2=VALUE2;FUNC={(FUNC_KEY1=FUNC_VALUE1,FUNC_KEY2=FUNC_VALUE2)};KEY3=VALUE3\\n"


with open(testf, 'r') as fi:
    # Read and print each subsequent line
    for line in fi:
        line = line.strip()
        if line.startswith("#"):
            continue
        #process_line(line)
        line = line.split('\t')
        col_info = line[7].split(";")
        func = [x for x in col_info if "FUNC=" in x]
        col_info =  [x for x in col_info if "FUNC=" not in x]
        print("HERE COMES FUNC")
        print(func)
        print("HERE COMES COL INFO")
        print(col_info)


# Process the example line
processed_data = process_line(example_line)

# Convert the dictionary to a pandas DataFrame
df = pd.DataFrame([processed_data])

# Print the DataFrame
print(df)

