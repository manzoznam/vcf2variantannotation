#!/usr/bin/python3 
import sys
import json

def parse_vcf_header(vcf_header):
    # Define your list of key words
    key_words = ["fileformat",
            "fileDate",
            "fileUTCtime",
            "source",
            "reference",
            "IonReporterAnalysisName",
            "IonReporterExportVersion",
            "IonReporterSoftwareVersion",
            "sampleGender",
            "IonReporterWorkflowName",
            "IonReporterWorkflowVersion",
            "AssumedGender",
            "CellularityAsAFractionBetween0-1",
            "basecallerVersion",
            "calculated_tumor_cellularity",
            "deamination_metric",
            "manually_input_percent_tumor_cellularity",
            "mapd",
            "median_reads_per_amplicon",
            "percent_aligned_reads",
            "percent_non_zero_amplicons",
            "sampleDiseaseType",
            "total_read_count"]
    # Initialize an empty dictionary to store key-value pairs
    metadata_dict = {}
    # Split the header by newline characters
    header_lines = vcf_header.split("\n")
    # Iterate through each line
    for line in header_lines:
        if line.startswith("##"):
            # Check if any key word is present in the line
            for key_word in key_words:
                if "tmap" not in line:
                    if key_word in line :
                        if line.count("=") == 1:
                            # Split the line into key and value
                            key, value = line[2:].split("=")
                            metadata_dict[key] = value
                            break  # Stop searching for key words once found
    return metadata_dict

if __name__ == "__main__":
    # Read input from stdin
    vcf_header = sys.stdin.read()
    # Call your parsing function
    parsed_metadata = parse_vcf_header(vcf_header)
    json.dump(parsed_metadata, sys.stdout, indent=4)  

