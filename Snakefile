#!/usr/bin/python

configfile: "config.yaml"

import os
import hashlib


def get_output_directory(wildcards):
    # Extract the relevant parts from the input path
    dirs = os.path.dirname(wildcards)
    return f"{dirs}"

def get_input_basename(wildcards):
	base_name = os.path.basename(wildcards)
	base_name = base_name.removesuffix(".vcf")
	return f"{base_name}"

#### hash function
def shorten_string(input_string, length=6):
    # Calculate the SHA-256 hash of the input string
    sha256_hash = hashlib.sha256(input_string.encode()).hexdigest()
    # Take the first 'length' characters from the hash
    shortened_digest = sha256_hash[:length]
    return shortened_digest

## Output variables
path_dir = get_output_directory(config['input_file'])
base_name = get_input_basename(config['input_file'])
#hash_string = shorten_string(base_name)
subset_string = base_name[-6:]
subset_string = subset_string.replace(":", "_")
vcf_pyrepair = f'{path_dir}/{base_name}.pyrepair.vcf'
py_hdr_parse = config['py_header_parse']
path_html = path_dir + "/VariantReport.html"
dir_annotations = f'{path_dir}/SMK_{subset_string}/Module_annotations/'

rule all:
	input:	f'{path_dir}/SMK_{subset_string}/metadata/vcf_metadata.json',
		f'{path_dir}/SMK_{subset_string}/Module_pyrepair/vcf_modifications.json',
		f'{path_dir}/SMK_{subset_string}/Module_pyrepair/vcf_metrics.json',
		f'{path_dir}/SMK_{subset_string}/.log/pyrepair.log',
		f'{path_dir}/SMK_{subset_string}/metadata/vcf_filterconfig.json',
		f'{path_dir}/{base_name}.pyrepair_bcf.vcf',
		f'{path_dir}/SMK_{subset_string}/Module_parse/snv.tsv',
		f'{path_dir}/SMK_{subset_string}/Module_parse/parsed_snv.tsv',
		f'{path_dir}/SMK_{subset_string}/Module_parse/parsed_cnv.tsv',
                f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_gnomad.tsv',
                f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_COSMIC.tsv',
		f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_cancerHotspot.tsv',
		f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_ClinVar.tsv',
		f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_TSG.tsv',
	        f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_oncogenicPositions.tsv',
                f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_MP_variant.tsv',
                f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_BRAF_variant_class.tsv',
                f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_HRR_genes.tsv',
                f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_horak.tsv'
                #f'{path_dir}/.log/varreport_update.done',
		#path_html,
		#f'{path_dir}/.log/Googlesheet_variantannotation_created.log'

print("### Check input vcf -- skipped -- REFACTOR pyrepair")
include: 'rules/vcf_repair.smk'

print("### Generate metadata from input VCF")
include: 'rules/metadata.smk'

#print("### Started gnomad section")
# include: 'rules/gnomad.smk'

print("### Started BCFtools based filtering")
include: 'rules/filter.smk'


print("### Started VCF parsing")
include: 'rules/parse.smk'

print("### Started Variant Annotation Module")
include: 'rules/annotations.smk'

print("### Started VariantReport.html")
include: 'rules/varreport.smk'

#print("### Started Googlesheet creation")
#include: 'rules/googlesheet_variantannotation.smk'

