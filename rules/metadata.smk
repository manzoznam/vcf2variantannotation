# Generate metadata


rule metadata:
	input: 	vcf=vcf_pyrepair,
		hdr_parse = py_hdr_parse 
	output: f'{path_dir}/SMK_{subset_string}/metadata/vcf_metadata.json'
	shell: "bcftools head {input.vcf} | python3 {input.hdr_parse} > {output}"


rule filter_conf:
	input: jsonfile = f'{path_dir}/SMK_{subset_string}/metadata/vcf_metadata.json',
		config_script = f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/filter_config.py'	
	output: f'{path_dir}/SMK_{subset_string}/metadata/vcf_filterconfig.json'
	shell: "python3 {input.config_script} {input.jsonfile} > {output}"

