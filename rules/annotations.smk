# SCOPE
# implement whole annotation pipeline into this Snakemake file

################################################### FUNCTION definition to generate JSON file with filepaths saved

ruleorder: annotation_horak > annotation_oncogenpos > annotation_tsg> annotation_clinvar > annotation_cancerhotspot > annotation_cosmic > annotation_gnomad
#####################################################

rule annotation_gnomad:
  input:
    snv=f'{path_dir}/SMK_{subset_string}/Module_parse/parsed_snv.tsv',
    rscript=f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/annotation_gnomad.R',    
  output:
    f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_gnomad.tsv',
  shell:
    """
    Rscript {input.rscript} -f {input.snv} -g {config[path_gnomad_sdb]} > {output}
    """



rule annotation_cosmic:
  input:
    snv=f'{path_dir}/SMK_{subset_string}/Module_parse/parsed_snv.tsv',
    rscript=f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/annotation_cosmic.R',
  output:
    f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_COSMIC.tsv'
  shell:
    """
    Rscript {input.rscript} -f {input.snv} -c {config[path_cosmic_sdb]} > {output}
    """

rule annotation_cancerhotspot:
  input:
    snv=f'{path_dir}/SMK_{subset_string}/Module_parse/parsed_snv.tsv',
    rscript=f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/annotation_cancerhotspot.R',
  output:                
    f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_cancerHotspot.tsv'
  shell:
    """
    Rscript {input.rscript} -f {input.snv} -c {config[path_cancerhotspot]} > {output}
    """

rule annotation_clinvar:
  input:
    snv=f'{path_dir}/SMK_{subset_string}/Module_parse/parsed_snv.tsv',
    rscript= f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/annotation_clinvar.R',
  params: json_key="clinvar"  
  output:		
    f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_ClinVar.tsv'
  shell:
    """
    Rscript {input.rscript} -f {input.snv} -c {config[path_clinvar_sdb]} > {output}
    """


rule annotation_tsg:
  input:
    snv=f'{path_dir}/SMK_{subset_string}/Module_parse/parsed_snv.tsv',
    rscript= f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/annotation_tsg.R',
  output:                
    f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_TSG.tsv'
  shell:
    """
    Rscript {input.rscript} -f {input.snv} -t {config[path_tsg_lengths]} > {output}
    """


rule annotation_oncogenpos:
  input:
    snv=f'{path_dir}/SMK_{subset_string}/Module_parse/parsed_snv.tsv',
    rscript= f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/annotation_oncogenpos.R',
  output:		
    f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_oncogenicPositions.tsv'
  shell:
    """
    Rscript {input.rscript} -f {input.snv} -o {config[path_oncogenicpositions]} > {output}
    """


rule annotation_braf:
  input:
    snv=f'{path_dir}/SMK_{subset_string}/Module_parse/parsed_snv.tsv',
    rscript= f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/annotation_braf.R',
  output:
    f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_BRAF_variant_class.tsv'
  shell:
    """
    Rscript {input.rscript} -f {input.snv} -g {config[googlesheets_braflink]} > {output}
    """

rule annotation_mp:
  input:
    snv=f'{path_dir}/SMK_{subset_string}/Module_parse/parsed_snv.tsv',
    rscript= f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/annotation_mp.R',
  output:
    f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_MP_variant.tsv'
  shell:
    """
    Rscript {input.rscript} -f {input.snv} -g {config[googlesheets_mpvarlink]} > {output}
    """

rule annotation_hrr:
  input:
    snv=f'{path_dir}/SMK_{subset_string}/Module_parse/parsed_snv.tsv',
    rscript= f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/annotation_hrr.R',
  output:
    f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_HRR_genes.tsv'
  shell:
    """
    Rscript {input.rscript} -f {input.snv} -g {config[googlesheets_hrrgenes]} > {output}
    """


rule annotation_horak:
  input:
    path_ch=f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_cancerHotspot.tsv',
    path_gnomad=f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_gnomad.tsv',
    path_tsg=f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_TSG.tsv',
    path_oncopos=f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_oncogenicPositions.tsv',
    rscript= f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/annotation_horak.R'
  output:
    f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_horak.tsv',
    #listings = f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_Horak_listings.tsv'
  shell:
    """
    Rscript {input.rscript} -d {dir_annotations} > {output}
    """
# path_annot_HorakScoreListings = paste0(annotation_dir, '/annotation_HorakScoreListings.tsv')


# googlesheets_cnv_onco: https://docs.google.com/spreadsheets/d/1dyu-WsczRwqJ57AUkpJieMz1AbpUy3BWOudXYV4a7mw/edit#gid=1301996234






