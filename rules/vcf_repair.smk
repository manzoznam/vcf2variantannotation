# Generate pyrepair

rule vcf_repair:
	input: 	vcf = config['input_file']
	output: pyrepair = f'{path_dir}/{base_name}.pyrepair.vcf',
		mods = f'{path_dir}/{base_name}.modifications.json',
                metrics= f'{path_dir}/{base_name}.metricsmerge.json',
		log = f'{path_dir}/SMK_{subset_string}/.log/pyrepair.log'
	run: 
		shell("python3 /home/watchdog/github_app/base_pythonrepairsvcf/vcfrepair.py {input.vcf} > {output.log}")

rule mv_repairs:
	input:	mods = f'{path_dir}/{base_name}.modifications.json',
                metrics= f'{path_dir}/{base_name}.metricsmerge.json',

	output:	mods = f'{path_dir}/SMK_{subset_string}/Module_pyrepair/vcf_modifications.json',
                metrics = f'{path_dir}/SMK_{subset_string}/Module_pyrepair/vcf_metrics.json',
	run:
                shell("mv {input.mods} {output.mods}")
                shell("mv {input.metrics} {output.metrics}")

