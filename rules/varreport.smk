

rule variantreport:
	input: vcf=f'{path_dir}/{base_name}.pyrepair_bcf.vcf', genie=f'/home/watchdog/github_app/genie2.0/genie.R'
	output: path_html, touch(f'{path_dir}/SMK_{subset_string}/.log/varreport_update.done')
        run:
		shell(f"Rscript {input.genie} -f {input.vcf} -o 'parse_annotation'")

