# Normalize the VCF file
import os
join_R = Rscript_join 

rule vcf_remove_CNV:
	input:	vcf_pyrepair	
	output: f'{path_dir}/SMK_{hash_string}/Module_gnomad_MAF/CNV_removed.vcf'
	shell: "bcftools view -e 'ALT=\"<CNV>\"' {input} >{output}"

rule vcf_prep:
	input:  f'{path_dir}/SMK_{hash_string}/Module_gnomad_MAF/CNV_removed.vcf'
	output: f'{path_dir}/SMK_{hash_string}/Module_gnomad_MAF/chr_removed.vcf'
	run:
		shell("bcftools norm -m - {input} | bcftools annotate --rename-chrs {config[chr_remove]} > {output}")

rule gnomad_input:
	input: f'{path_dir}/SMK_{hash_string}/Module_gnomad_MAF/chr_removed.vcf'
	output: f'{path_dir}/SMK_{hash_string}/Module_gnomad_MAF/gnomad_input.tsv'
	run:
		shell('bcftools query -f "%CHROM\t%POS\t%REF\t%ALT" {input} > {output}')

rule gnomad_output:
	input: f'{path_dir}/SMK_{hash_string}/Module_gnomad_MAF/gnomad_input.tsv'
	output: f'{path_dir}/SMK_{hash_string}/Module_gnomad_MAF/gnomad_output.tsv'
	run: 
		shell("tabix -R {input} {config[tabix_gnomad]} > {output}")

rule extract_gnomad_MAF:
        input: 	gin=f'{path_dir}/SMK_{hash_string}/Module_gnomad_MAF/gnomad_input.tsv',
         	gout=f'{path_dir}/SMK_{hash_string}/Module_gnomad_MAF/gnomad_output.tsv'
	output: log=f'{path_dir}/SMK_{hash_string}/Module_gnomad_MAF/R_join.log',
		annot=f'{path_dir}/SMK_{hash_string}/Module_gnomad_MAF/Annotfile.tsv'
	run:
		shell("Rscript {join_R} --gnomadin {input.gin} --gnomadout {input.gout} > {output.log}")

rule bgzip_annotate:
	input: f'{path_dir}/SMK_{hash_string}/Module_gnomad_MAF/Annotfile.tsv'
	output: annot=f'{path_dir}/SMK_{hash_string}/Module_gnomad_MAF/Annotfile.tsv.gz',
		tbi=f'{path_dir}/SMK_{hash_string}/Module_gnomad_MAF/Annotfile.tsv.gz.tbi'
	run:
		shell("bgzip -f {input} > {output.annot}")
		shell("tabix -f -s 1 -b 2 -e 2 {output.annot} > {output.tbi}")

rule annotate_vcf:
	input:  annotfile=f'{path_dir}/SMK_{hash_string}/Module_gnomad_MAF/Annotfile.tsv.gz',
		origvcf= vcf_pyrepair
	output: f'{path_dir}/SMK_{hash_string}/Module_gnomad_MAF/{base_name}.gnomadMAF.vcf'
	run:
		shell("bcftools annotate -a {input.annotfile} -h {config[gnomad_hdr]} -c 'CHROM,POS,REF,ALT,gnomad_MAF' {input.origvcf} > {output}")
