

rule googlesheet:
	input: 	dir=path_dir,
		touched=f'{path_dir}/.log/varreport_update.done',
		bridge=f'/home/watchdog/github_app/bridge_VariantReport_GSheet/scripts/bridge_VR_GS.R'
	output: touch(f'{path_dir}/.log/Googlesheet_variantannotation_created.log')
        run:
		shell(f"Rscript {input.bridge} -d {input.dir}")

