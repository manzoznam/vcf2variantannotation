import json
def load_json_config(json_path):
    with open(json_path, 'r') as json_file:
        config = json.load(json_file)
        return config['AF_filter']

rule bcf_filter:
        input: pyrepairvcf = f'{path_dir}/{base_name}.pyrepair.vcf',
                filter_config = f'{path_dir}/SMK_{subset_string}/metadata/vcf_filterconfig.json'
        output: f'{path_dir}/{base_name}.pyrepair_bcf.vcf'
        params: af_filter=lambda wildcards, input: load_json_config(input.filter_config)
        shell:
                """
                bcftools norm -m - {input.pyrepairvcf} | bcftools view -i '(INFO/AF > {params.af_filter})' > {output}
                """

# bcftools view -i '(INFO/AF> {params.af_filter}) || (CHROM=="chr7" && POS>=116411800 && POS<=116412140 && INFO/AF >0)' {input.pyrepairvcf}  > {output}

rule gnomad_filter:
	input:f'{path_dir}/{base_name}.pyrepair_bcf.vcf'	
	output: f'{path_dir}/SMK_{subset_string}/Module_parse/snv.tsv'
 	shell: "bcftools view -e 'ALT=\"<CNV>\"' {input}  > {output}"

rule cnv:
	input:  f'{path_dir}/{base_name}.pyrepair_bcf.vcf'
        output: f'{path_dir}/SMK_{subset_string}/Module_parse/cnv.tsv'
	shell: "bcftools view -i 'ALT=\"<CNV>\"' {input} > {output}"

