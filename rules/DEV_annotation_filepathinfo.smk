#!/usr/bin/python

ruleorder: annotation_json_gnomad > annotation_json_cosmic > annotation_json_cancerhotspot > annotation_json_clinvar > annotation_json_tsg > annotation_json_oncogenpos 

rule annotation_json_gnomad:
  input:
    fp=f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_gnomad.tsv',
    updatejson=f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/annotation_update_json.py',
  params: json_key="gnomad"
  output:
    f'{path_dir}/SMK_{subset_string}/Module_annotations/filepath_info.json'
  shell:
    """
    python3 {input.updatejson} {output} {input.fp} {params.json_key}
    """


rule annotation_json_cosmic:
  input:
    fp=f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_COSMIC.tsv',
    updatejson=f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/annotation_update_json.py',
  params: json_key="cosmic"
  output:
    f'{path_dir}/SMK_{subset_string}/Module_annotations/filepath_info.json'
  shell:
    """
    python3 {input.updatejson} {output} {input.fp} {params.json_key}
    """

rule annotation_json_cancerhotspot:
  input:
    fp=f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_cancerHotspot.tsv',
    updatejson=f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/annotation_update_json.py',
  params: json_key="cancerHotspot"
  output:
    f'{path_dir}/SMK_{subset_string}/Module_annotations/filepath_info.json'
  shell:
    """
    python3 {input.updatejson} {output} {input.fp} {params.json_key}
    """

rule annotation_json_clinvar:
  input:
    fp=f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_ClinVar.tsv',
    updatejson=f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/annotation_update_json.py',
  params: json_key="clinvar"
  output:
    f'{path_dir}/SMK_{subset_string}/Module_annotations/filepath_info.json'
  shell:
    """
    python3 {input.updatejson} {output} {input.fp} {params.json_key}
    """

rule annotation_json_tsg:
  input:
    fp=f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_TSG.tsv',
    updatejson=f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/annotation_update_json.py',
  params: json_key="TSG"
  output:
    f'{path_dir}/SMK_{subset_string}/Module_annotations/filepath_info.json'
  shell:
    """
    python3 {input.updatejson} {output} {input.fp} {params.json_key}
    """

rule annotation_json_oncogenpos:
  input:
    fp=f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_oncogenicPositions.tsv',
    updatejson=f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/annotation_update_json.py',
  params: json_key="oncogenpos"
  output:
    f'{path_dir}/SMK_{subset_string}/Module_annotations/filepath_info.json'
  shell:
    """
    python3 {input.updatejson} {output} {input.fp} {params.json_key}
    """


rule annotation_horak:
  input:
    fp=f'{path_dir}/SMK_{subset_string}/Module_annotations/filepath_info.json',
    rscript= f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/annotation_horak.R'
  output:
    f'{path_dir}/SMK_{subset_string}/Module_annotations/annotation_horak.tsv'
  shell:
    """
    Rscript {input.rscript} -f > {output}
    """
# path_annot_HorakScoreListings = paste0(annotation_dir, '/annotation_HorakScoreListings.tsv')

