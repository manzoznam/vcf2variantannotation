rule parse_intermediates:
    input:
        snv= f'{path_dir}/SMK_{subset_string}/Module_parse/snv.tsv',
        cnv= f'{path_dir}/SMK_{subset_string}/Module_parse/cnv.tsv',
        parser=f'/home/watchdog/github_app/snakemake_workflows/vcf2variantannotation/scripts/vcf_parse.R'
    output:
        snv= f'{path_dir}/SMK_{subset_string}/Module_parse/parsed_snv.tsv',
        cnv= f'{path_dir}/SMK_{subset_string}/Module_parse/parsed_cnv.tsv',
    shell:
        """
        Rscript {input.parser} -f {input.snv} > {output.snv}
        Rscript {input.parser} -f {input.cnv} > {output.cnv}
        """
